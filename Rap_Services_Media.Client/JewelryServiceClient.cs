﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.RestClient;
using Rap_Services_Media.Client.Interfaces;

namespace Rap_Services_Media.Client
{
    public class JewelryServiceClient : ClientApi, IMediaServiceClient
    {
        private const string ApiPath = "Jewelry";
        public JewelryServiceClient(string serviceUri) : base(serviceUri)
        { }

    }
}
