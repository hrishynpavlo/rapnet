﻿namespace RapServices_Media.Api.Constants
{
    public static class MediaTypes
    {
        public const string Image = "Image";
        public const string Video = "Video";
    }

    public static class RapTypes
    {
        public const string Jewelry = "JewelryApi";
    }
    
    public static class ImageResolutions
    {
        public const string Original = "original";
        public const string Small = "small";
        public const string Large = "large";
        public const string Thumbnail = "thumbnail";
    }
}
