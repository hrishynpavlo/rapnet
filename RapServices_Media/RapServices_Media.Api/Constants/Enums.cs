﻿namespace RapServices_Media.Api.Constants.Enums
{
    public enum MediaType
    {
        image = 1,
        video,
        pdf, 
        doc
        //etc    
    }

    public enum ImageSizeAlias
    {
        original = 1,
        small,
        large,
        thumbnail
        //etc
    }
}
