﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace RapServices_Media.Api.Migrations
{
    public partial class media : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "mediaDB");

            migrationBuilder.CreateTable(
                name: "Media",
                schema: "mediaDB",
                columns: table => new
                {
                    MediaId = table.Column<string>(nullable: false),
                    MediaType = table.Column<string>(nullable: true),
                    Metadata = table.Column<string>(nullable: true),
                    RapType = table.Column<string>(nullable: true),
                    MediaName = table.Column<string>(nullable: true),
                    AccountId = table.Column<int>(nullable: false),
                    ContactId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Media", x => x.MediaId);
                });

            migrationBuilder.CreateTable(
                name: "VideoProvider",
                schema: "mediaDB",
                columns: table => new
                {
                    ProviderId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoProvider", x => x.ProviderId);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                schema: "mediaDB",
                columns: table => new
                {
                    ImageId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MediaId = table.Column<string>(nullable: true),
                    Resolution = table.Column<string>(nullable: true),
                    Hight = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Link = table.Column<string>(nullable: true),
                    FileType = table.Column<string>(nullable: true),
                    MimeType = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.ImageId);
                    table.ForeignKey(
                        name: "FK_Image_Media_MediaId",
                        column: x => x.MediaId,
                        principalSchema: "mediaDB",
                        principalTable: "Media",
                        principalColumn: "MediaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Video",
                schema: "mediaDB",
                columns: table => new
                {
                    VideoId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MediaId = table.Column<string>(nullable: true),
                    ProviderId = table.Column<int>(nullable: false),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Video", x => x.VideoId);
                    table.ForeignKey(
                        name: "FK_Video_Media_MediaId",
                        column: x => x.MediaId,
                        principalSchema: "mediaDB",
                        principalTable: "Media",
                        principalColumn: "MediaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Video_VideoProvider_ProviderId",
                        column: x => x.ProviderId,
                        principalSchema: "mediaDB",
                        principalTable: "VideoProvider",
                        principalColumn: "ProviderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Image_MediaId",
                schema: "mediaDB",
                table: "Image",
                column: "MediaId");

            migrationBuilder.CreateIndex(
                name: "IX_Video_MediaId",
                schema: "mediaDB",
                table: "Video",
                column: "MediaId");

            migrationBuilder.CreateIndex(
                name: "IX_Video_ProviderId",
                schema: "mediaDB",
                table: "Video",
                column: "ProviderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Image",
                schema: "mediaDB");

            migrationBuilder.DropTable(
                name: "Video",
                schema: "mediaDB");

            migrationBuilder.DropTable(
                name: "Media",
                schema: "mediaDB");

            migrationBuilder.DropTable(
                name: "VideoProvider",
                schema: "mediaDB");
        }
    }
}
