﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Interfaces;

namespace RapServices_Media.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideoProvidersController : ControllerBase
    {
        private readonly ILogger<MediaController> _logger;
        private readonly IVideoLinksManager _manager;

        public VideoProvidersController(ILogger<MediaController> logger, IVideoLinksManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [HttpPost]
        [Route("Accounts/{accountId}/Contacts/{contactId}/VideoLink")]
        public async Task<MediaResponseModelDTO> AddVideoLink(int accountId, int contactId, [FromBody]string link)
        {
            try
            {
                if (!string.IsNullOrEmpty(link))
                {
                    var mediaId = await _manager.AddVideoLink(accountId, contactId, link);
                    return new MediaResponseModelDTO(ErrorCodes.NoError, mediaId);
                }
                return new MediaResponseModelDTO(ErrorCodes.VideoLinkIsNullOrEmpty);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpPut]
        [Route("Accounts/{accountId}/Contacts/{contactId}/VideoLink")]
        public async Task<MediaResponseModelDTO> UpdateVideoLink(int accountId, int contactId, [FromBody]VideoLinkRequestDTO link)
        {
            try
            {
                if (string.IsNullOrEmpty(link.Id))
                {
                    await _manager.UpdateVideoLink(accountId, contactId, link);
                    return new MediaResponseModelDTO(ErrorCodes.NoError);
                }
                return new MediaResponseModelDTO(ErrorCodes.ItemDoesntExist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpDelete]
        [Route("Accounts/{accountId}/Contacts/{contactId}/VideoLink/{mediaItemId}")]
        public async Task<MediaResponseModelDTO> RemoveVideoLink(int accountId, int contactId, [FromBody]VideoLinkRequestDTO link)
        {
            try
            {
                if (string.IsNullOrEmpty(link.Id))
                {
                    await _manager.RemoveVideoLink(accountId, contactId, link.Id);
                    return new MediaResponseModelDTO(ErrorCodes.NoError);
                }
                return new MediaResponseModelDTO(ErrorCodes.ItemDoesntExist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpGet]
        [Route("Accounts/{accountId}/Contacts/{contactId}/VideoLink/{mediaItemId}")]
        public async Task<MediaResponseModelDTO> GetMediaIds(int accountId, int contactId, string mediaItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(mediaItemId))
                {
                    var response = await _manager.GetVideoLink(accountId, contactId, mediaItemId);
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.InvalidRequestModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

    }
}