﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Interfaces;

namespace RapServices_Media.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly ILogger<MediaController> _logger;
        private readonly IMediaServiceManager _manager;

        /// <summary>
        /// Constructor for MediaController
        /// </summary>
        /// <param name="logger">ILogger<MediaController></param>
        /// <param name="manager">IMediaServiceManager</param>
        /// <returns>MediaController object</returns>
        public MediaController(ILogger<MediaController> logger, IMediaServiceManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        /// <summary>
        /// POST Method for asynchronus uploading of image 
        /// </summary>
        /// <param name="accountId">int</param>
        /// <param name="contactId">int</param>
        /// <param name="item">[FromForm]MediaItemDTO</param>
        /// <returns>async Task<MediaResponseModelDTO> result</returns>
        [HttpPost]
        [Route("Accounts/{accountId}/Contacts/{contactId}/Image")]
        public async Task<MediaResponseModelDTO> UploadImage(int accountId, int contactId, [FromForm]MediaItemDTO item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _manager.UploadImageAsync(accountId, contactId, item);
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.InvalidRequestModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Server side issue");
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex.Message);
            }
        }

        /// <summary>
        /// PUT Method for asynchronus updating of image 
        /// </summary>
        /// <param name="accountId">int</param>
        /// <param name="contactId">int</param>
        /// <param name="mediaItemId">string</param>
        /// <param name="item">[FromForm]MediaItemDTO</param>
        /// <returns>async Task<MediaResponseModelDTO> result</returns>
        [HttpPut]
        [Route("Accounts/{accountId}/Contacts/{contactId}/Image/{mediaItemId}")]
        public async Task<MediaResponseModelDTO> UpdateFile(int accountId, int contactId, string mediaItemId, [FromForm]MediaItemDTO item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _manager.UpdateImageAsync(accountId, contactId, mediaItemId, item);
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.InvalidRequestModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Server side issue");
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex.Message);
            }
        }

        /// <summary>
        /// DELETE Method for asynchronus removing of image 
        /// </summary>
        /// <param name="accountId">int</param>
        /// <param name="contactId">int</param>
        /// <param name="mediaItemId">string</param>
        /// <returns>async Task<MediaResponseModelDTO> result</returns>
        [HttpDelete]
        [Route("Accounts/{accountId}/Contacts/{contactId}/Image/{mediaItemId}")]
        public async Task<MediaResponseModelDTO> RemoveFile(int accountId, int contactId, string mediaItemId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _manager.RemoveImageAsync(mediaItemId);
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.ItemDoesntExist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Server side issue");
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex.Message);
            }
        }

        /// <summary>
        /// GET Method for asynchronus getting sets of images by media
        /// </summary>
        /// <param name="accountId">int</param>
        /// <param name="contactId">int</param>
        /// <param name="mediaItemId">string</param>
        /// <returns>async Task<MediaResponseModelDTO> result</returns>
        [HttpGet]
        [Route("Accounts/{accountId}/Contacts/{contactId}/Image/{mediaItemId}")]
        public async Task<MediaResponseModelDTO> GetMediaIds(int accountId, int contactId, string mediaItemId) {
            try
            {
                if (ModelState.IsValid)
                {
                    object response = new object();
                    await Task.Run(() => {
                        response = _manager.GetMediaLink(accountId, contactId, mediaItemId);
                    });
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.InvalidRequestModel);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Server side issue");
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex.Message);
            }
        }

        /// <summary>
        /// POST Method for asynchronus getting sets of images by set of media
        /// </summary>
        /// <param name="accountId">int</param>
        /// <param name="contactId">int</param>
        /// <param name="searchParams">MediaRequestModel</param>
        /// <returns>async Task<MediaResponseModelDTO> result</returns>
        [HttpPost]
        [Route("Accounts/{accountId}/Contacts/{contactId}/Images")]
        public async Task<MediaResponseModelDTO> GetMediasIds(int accountId, int contactId, [FromBody]MediaRequestModel searchParams)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    object response = new object();
                    await Task.Run(() => {
                        response = _manager.GetMediaLinks(accountId, contactId, searchParams);
                    });
                    return new MediaResponseModelDTO(ErrorCodes.NoError, response);
                }
                return new MediaResponseModelDTO(ErrorCodes.InvalidRequestModel);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Server side issue");
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex.Message);
            }
        }
    }
}