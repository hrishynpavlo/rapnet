﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Interfaces;

namespace RapServices_Media.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideoLinksController : ControllerBase
    {
        private readonly ILogger<MediaController> _logger;
        private readonly IVideoLinksManager _manager;

        public VideoLinksController(ILogger<MediaController> logger, IVideoLinksManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [HttpPost]
        [Route("Provider")]
        public async Task<MediaResponseModelDTO> AddProvider([FromBody]VideoProviderRequestDTO provider)
        {
            try
            {
                await _manager.AddProvider(provider.ProviderLink);
                return new MediaResponseModelDTO(ErrorCodes.NoError);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpPut]
        [Route("Provider")]
        public async Task<MediaResponseModelDTO> UpdateProvider([FromBody]VideoProviderRequestDTO provider)
        {
            try
            {
                if (provider.Id != 0)
                {
                    await _manager.UpdateProvider(provider);
                    return new MediaResponseModelDTO(ErrorCodes.NoError);
                }
                return new MediaResponseModelDTO(ErrorCodes.ItemDoesntExist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpDelete]
        [Route("Provider")]
        public async Task<MediaResponseModelDTO> RemoveProvider([FromBody]VideoProviderRequestDTO provider)
        {
            try
            {
                if (provider.Id != 0)
                {
                    await _manager.RemoveProvider(provider.Id);
                    return new MediaResponseModelDTO(ErrorCodes.NoError);
                }
                return new MediaResponseModelDTO(ErrorCodes.ItemDoesntExist);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }

        [HttpGet]
        [Route("Providers")]
        public async Task<MediaResponseModelDTO> GetProviders()
        {
            try
            {
                var response = await _manager.GetProviders();
                return new MediaResponseModelDTO(ErrorCodes.NoError, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new MediaResponseModelDTO(ErrorCodes.UnknownError, ex);
            }
        }
    }
}