﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.DTO
{
    public class VideoProviderRequestDTO
    {
        public short Id { get; set; }
        public string ProviderLink { get; set; }
    }
}
