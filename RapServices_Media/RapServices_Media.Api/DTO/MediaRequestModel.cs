﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.DTO
{
    public class MediaRequestModel
    {
        public List<MediaListRequestModel> MediaAliasIds { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
    public class MediaListRequestModel
    {
        public string MediaId { get; set; }
        public List<string> AliasIds { get; set; }
    }
}
