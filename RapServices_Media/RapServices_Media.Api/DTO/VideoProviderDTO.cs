﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.DTO
{
    public class VideoProviderDTO
    {
        public int Id { get; set; }
        public string Link { get; set; }
    }
}
