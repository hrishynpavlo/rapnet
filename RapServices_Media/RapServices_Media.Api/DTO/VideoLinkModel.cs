﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.DTO
{
    public class VideoLinkModel
    {
        public string MediaId { get; set; }
        public string VideoLink { get; set; }
        public bool Result { get; set; }
    }
}
