﻿namespace RapServices_Media.Api.DTO
{
    public class MediaResponseModelDTO
    {
        public MediaResponseModelDTO() { }

        public MediaResponseModelDTO(ErrorCodes errorCode)
        {
            Error = (int)errorCode;
        }

        public MediaResponseModelDTO(ErrorCodes errorCode, object data)
        {
            Error = (int)errorCode;
            Data = data;
        }

        public int Error { get; set; }
        public object Data { get; set; }
    }

    public enum ErrorCodes
    {
        NoError = 0,
        UnknownError = 555000,
        InvalidRequestModel = 555002,

        EntityNotFound = 555004,
        FileFormatInvalid = 555008,
        ErrorWhileSaving = 555011,
        ItemExists = 555012,
        DatabaseException = 555017,
        S3Exception = 555018,
        FileDidntUpload = 555019,
        ItemDoesntExist = 555020,
        RequestDoesntContainsFile = 555021,
        VideoLinkIsNullOrEmpty = 555022
    }
}
