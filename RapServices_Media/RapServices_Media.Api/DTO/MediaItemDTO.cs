﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using RapServices_Media.Api.Attributes.Binders;

namespace RapServices_Media.Api.DTO
{  
    [ModelBinder(BinderType = typeof(MediaItemBinder))]
    public class MediaItemDTO
    {
        public IFormFile File { get; set; }
        public Dictionary<string, Size> Sizes { get; set; }
        public string RapType { get; set; }
        public string MediaName { get; set; }
        public object Metadata { get; set; }
	}
    public class Size
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public override string ToString()
        {
            return $"The size is {Width} x {Height} px (width x heiht format)";
        }
    }
}
