﻿using System.Collections.Generic;
using RapServices_Media.Api.DTO;
using System.Threading.Tasks;
using RapServices_Media.Api.Accessors.DbAccessor;
using RapServices_Media.Api.Accessors.DbAccessor.DO;

namespace RapServices_Media.Api.Interfaces
{
    public interface IVideoLinksDbAccessor
    {
        Task<bool> SaveProviderAsync(string providerLink);
        Task<bool> EditProviderAsync(VideoProviderRequestDTO provider);
        Task<bool> DeleteProviderAsync(short providerId);
        Task<List<VideoProvider>> GetProviders();

        Task<DbResultModel> SaveLinkAsync(int accountId, int contactId, string link);
        Task<bool> EditLinkAsync(int accountId, int contactId, VideoLinkRequestDTO link);
        Task<bool> DeleteLinkAsync(int accountId, int contactId, string linkId);
        Task<VideoLinkModel> GetVideoLink(int accountId, int contactId, string mediaItemId);
    }
}
