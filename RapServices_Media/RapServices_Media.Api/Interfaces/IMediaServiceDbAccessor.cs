﻿using RapServices_Media.Api.Accessors.DbAccessor;
using RapServices_Media.Api.Accessors.StorageAccessor;
using RapServices_Media.Api.DTO;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Interfaces
{
    public interface IMediaServiceDbAccessor
    {
        Task<DbResultModel> SaveImageAsync(int accountId, int contactId, MediaItemDTO item, StorageResultModel storageResult);
        Task<DbResultModel> EditImageAsync(string mediaItemId, MediaItemDTO item, StorageResultModel storageResult);
        Task<DbResultModel> DeleteImageAsync(string mediaItemId);
        MediaLinks GetSingleMediaImageLinks(string mediaItemId);
        ListMediaLinks GetMultipleImagesLinks(MediaRequestModel mediaItemIds);
    }
}
