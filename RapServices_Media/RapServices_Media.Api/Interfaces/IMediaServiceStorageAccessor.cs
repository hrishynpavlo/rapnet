﻿using RapServices_Media.Api.Accessors.StorageAccessor;
using RapServices_Media.Api.DTO;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Interfaces
{
    public interface IMediaServiceStorageAccessor
    {
		Task<StorageResultModel> UploadImageAsync(MediaItemDTO mediaItem, string mediaItemId = null);
        Task<RemoveStorageResultModel> RemoveImagesAsync(string mediaItem);
	}
}
