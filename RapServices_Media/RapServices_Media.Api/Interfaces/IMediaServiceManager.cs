﻿using System;
using System.Threading.Tasks;
using RapServices_Media.Api.DTO;

namespace RapServices_Media.Api.Interfaces
{
    public interface IMediaServiceManager
    {
        Task<object> UploadImageAsync(int accountId, int contactId, MediaItemDTO item);
        Task<object> UpdateImageAsync(int accountId, int contactId, string mediaItemId, MediaItemDTO item);
        Task<object> RemoveImageAsync(string mediaItemId);
        object GetMediaLinks(int accountId, int contactId, MediaRequestModel requestModel);
        object GetMediaLink(int accountId, int contactId, string mediaItemId);
    }
}
