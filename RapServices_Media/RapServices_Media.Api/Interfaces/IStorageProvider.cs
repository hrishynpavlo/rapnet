﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Interfaces
{
    public interface IStorageProvider
    {
        string BucketName { get; }
        string Url { get; }
        string AccessKey { get; }
        string SecretKey { get; }
    }
}
