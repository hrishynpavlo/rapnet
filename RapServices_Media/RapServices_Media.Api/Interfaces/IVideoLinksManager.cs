﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RapServices_Media.Api.Accessors.DbAccessor.DO;
using RapServices_Media.Api.DTO;

namespace RapServices_Media.Api.Interfaces
{
    public interface IVideoLinksManager
    {
        Task AddProvider(string providerLink);
        Task UpdateProvider(VideoProviderRequestDTO provider);
        Task RemoveProvider(short providerId);
        Task<List<VideoProviderDTO>> GetProviders();

        Task<string> AddVideoLink(int accountId, int contactId, string link);
        Task UpdateVideoLink(int accountId, int contactId, VideoLinkRequestDTO link);
        Task RemoveVideoLink(int accountId, int contactId, string linkId);
        Task<VideoLinkModel> GetVideoLink(int accountId, int contactId, string mediaId);
    }
}
