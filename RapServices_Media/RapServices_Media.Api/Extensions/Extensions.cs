﻿using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Http;
using ImageMagick;
using System;
using System.Threading;

namespace RapServices_Media.Api.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Return image height from bytes 
        /// </summary>
        /// <param name="byteArray">this byte[]</param>
        /// <returns>decimal height</returns>
        public static int GetHeight(this byte[] byteArray)
        {
            int height = 0;
            using (var ms = new MemoryStream(byteArray))
            {
                var image = Image.FromStream(ms);
                height = image.Height;
            }
            return height; 
        }

        /// <summary>
        /// Return image width from bytes 
        /// </summary>
        /// <param name="byteArray">this byte[]</param>
        /// <returns>decimal width</returns>
        public static int GetWidth(this byte[] byteArray)
        {
            int width = 0;
            using (var ms = new MemoryStream(byteArray))
            {
                var image = Image.FromStream(ms);
                width = image.Width;
            }
            return width;
        }

        /// <summary>
        /// Return bytes from file asynchronus  
        /// </summary>
        /// <param name="file">(this IFormFile</param>
        /// <returns>async Task byte[] bytes</returns>
        public static async Task<byte[]> GetBytesFromIFileAsync(this IFormFile file)
        {
            byte[] fileBytes;

            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                fileBytes = ms.ToArray();
            }
            return fileBytes;
        }

        /// <summary>
        /// Return file type by content type
        /// </summary>
        /// <param name="contentType">(this string)</param>
        /// <returns>string fileType</returns>
        public static string GetFileTypeByContentType(this string contentType)
        {
            return contentType.Remove(contentType.IndexOf("/"));
        }

        /// <summary>
        /// Return resized image by size parameter asynchronus
        /// </summary>
        /// <param name="originalImage">(this MagickImage)</param>
        /// <param name="size">(DTO.Size)</param>
        /// <returns>Task<MemoryStream> resizedImage</returns>
        public static async Task<MemoryStream> ResizeImageAsync(this MagickImage originalImage, DTO.Size size)
        {
            try
            {
                return await Task.Run(() => {
                    using (var image = originalImage.Clone())
                    {
                        var resultStream = new MemoryStream();
                        image.Resize(size.Width, size.Height);
                        image.Write(resultStream);
                        return resultStream;
                    }
                });
            }
           catch(Exception ex)
            {
                throw new Exception($"Resize was failed for {size.ToString()}", ex);
            }
        }
    }
}
