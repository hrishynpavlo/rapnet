﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Extensions;
using RapServices_Media.Api.Interfaces;

namespace RapServices_Media.Api.Managers
{
	public class MediaManager: IMediaServiceManager
	{
		private readonly ILogger<MediaManager> _logger;
		private readonly IMediaServiceDbAccessor _dbAccessor;
		private readonly IMediaServiceStorageAccessor _storageAccessor;

		public MediaManager(IMediaServiceDbAccessor dbAccessor, IMediaServiceStorageAccessor storageAccessor, ILogger<MediaManager> logger)
		{
			_dbAccessor = dbAccessor;
			_storageAccessor = storageAccessor;
			_logger = logger;
		}

        public object GetMediaLink(int accountId, int contactId, string mediaItemId)
        {
            try
            {
                var dbResult = _dbAccessor.GetSingleMediaImageLinks(mediaItemId);
                if (!dbResult.Result)
                {
                    _logger.LogInformation("Getting item from db was failed");
                    throw new Exception("Getting item from db was failed");
                }

                return dbResult;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Getting item was failed");
                throw new Exception("Getting item was failed", ex);
            }
        }

        public object GetMediaLinks(int accountId, int contactId, MediaRequestModel requestModel)
        {
            try
            {
                var dbResult = _dbAccessor.GetMultipleImagesLinks(requestModel);

                if (!dbResult.Result)
                {
                    _logger.LogInformation("Getting items from db was failed");
                    throw new Exception("Getting items from db was failed");
                }

                return dbResult;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Getting items was failed");
                throw new Exception("Getting items was failed", ex);
            }
        }

        public async Task<object> RemoveImageAsync(string mediaItemId)
        {
			try
			{
                var storageResult = await _storageAccessor.RemoveImagesAsync(mediaItemId);
                if (!storageResult.Result)
                {
                    _logger.LogInformation("Removing from bucket was failed");
                    throw new Exception("Removing from bucket was failed");
                }

                var dbResult = await _dbAccessor.DeleteImageAsync(mediaItemId);
                if (!dbResult.Result)
                {
                    _logger.LogInformation("Removing from db was failed");
                    throw new Exception("Removing from db was failed");
                }
                return dbResult;
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Removing was failed");
                throw new Exception("Removing was failed", ex);
			}
		}

        public async Task<object> UpdateImageAsync(int accountId, int contactId, string mediaItemId, MediaItemDTO item)
		{
			try
			{
                var uploadResult = await _storageAccessor.UploadImageAsync(item, mediaItemId);

                if (!uploadResult.Result)
                {
                    _logger.LogInformation("Uploading file to bucket was failed");
                    throw new Exception("Uploading file to bucket was failed");
                }

                var dbResult = await _dbAccessor.EditImageAsync(mediaItemId, item, uploadResult);
                if (!dbResult.Result)
                {
                    _logger.LogInformation("Editing item to db was failed");
                    throw new Exception("Editing item to db was failed");
                }

                return dbResult;
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Editing was failed");
                throw new Exception("Editing was failed", ex);
            }
		}

		public async Task<object> UploadImageAsync(int accountId, int contactId, MediaItemDTO item)
		{
            try
            {
                var uploadResult = await _storageAccessor.UploadImageAsync(item);

                if (!uploadResult.Result)
                {
                    _logger.LogInformation("Uploading file to bucket was failed");
                    throw new Exception("Uploading file to bucket was failed");
                }

                var dbResult = await _dbAccessor.SaveImageAsync(accountId, contactId, item, uploadResult);
                if (!dbResult.Result)
                {
                    _logger.LogInformation("Adding item to db was failed");
                    throw new Exception("Adding item to db was failed");
                }
                return dbResult; 
            }
			catch(Exception ex)
            {
                _logger.LogError(ex, "Uploading was failed");
                throw new Exception("Uploading was failed", ex);
            }
		}
	}
}
