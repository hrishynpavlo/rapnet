﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.Accessors.DbAccessor.DO;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Interfaces;

namespace RapServices_Media.Api.Managers
{
    public class VideoLinksManager : IVideoLinksManager
    {
        private readonly ILogger<MediaManager> _logger;
        private readonly IVideoLinksDbAccessor _dbAccessor;

        public VideoLinksManager(IVideoLinksDbAccessor dbAccessor, ILogger<MediaManager> logger)
        {
            _dbAccessor = dbAccessor;
            _logger = logger;
        }

        public async Task AddProvider(string providerLink)
        {
            try
            {
                var dbResult = await _dbAccessor.SaveProviderAsync(providerLink);
                if (!dbResult)
                {
                    _logger.LogInformation("Adding provider url to db was failed");
                    throw new Exception("Adding provider url to db was failed");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during saving provider link");
                throw new Exception("Error during saving provider link", ex);
            }
        }

        public async Task UpdateProvider(VideoProviderRequestDTO provider)
        {
            try
            {
                var dbResult = await _dbAccessor.EditProviderAsync(provider);
                if (!dbResult)
                {
                    _logger.LogInformation("Updating provider url in db was failed");
                    throw new Exception("Updating provider url in db was failed");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during updating provider link");
                throw new Exception("Error during updating provider link", ex);
            }
        }

        public async Task RemoveProvider(short providerId)
        {
            try
            {
                var dbResult = await _dbAccessor.DeleteProviderAsync(providerId);
                if (!dbResult)
                {
                    _logger.LogInformation("Deleting provider url in db was failed");
                    throw new Exception("Deleting provider url in db was failed");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during deleting provider link");
                throw new Exception("Error during deletingprovider link", ex);
            }
        }

        public async Task<List<VideoProviderDTO>> GetProviders()
        {
            try
            {
                var dbResult = await _dbAccessor.GetProviders();
                return dbResult.Select(x => new VideoProviderDTO { Id = x.ProviderId, Link = x.Name }).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting providers");
                throw new Exception("Error while getting providers", ex);
            }
        }


        public async Task<string> AddVideoLink(int accountId, int contactId, string link)
        {
            try
            {
                var dbResult = await _dbAccessor.SaveLinkAsync(accountId, contactId, link);
                if (!dbResult.Result)
                {
                    _logger.LogInformation("Adding video link to db was failed");
                    throw new Exception("Adding video link to db was failed");
                }
                return dbResult.MediaId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during saving video link");
                throw new Exception("Error during saving video link", ex);
            }
        }

        public async Task UpdateVideoLink(int accountId, int contactId, VideoLinkRequestDTO link)
        {
            try
            {
                var dbResult = await _dbAccessor.EditLinkAsync(accountId, contactId, link);
                if (!dbResult)
                {
                    _logger.LogInformation("Updating video link in db was failed");
                    throw new Exception("Updating video link in db was failed");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during updating video link");
                throw new Exception("Error during updating video link", ex);
            }
        }

        public async Task RemoveVideoLink(int accountId, int contactId, string link)
        {
            try
            {
                var dbResult = await _dbAccessor.DeleteLinkAsync(accountId, contactId, link);
                if (!dbResult)
                {
                    _logger.LogInformation("Deleting video link in db was failed");
                    throw new Exception("Deleting video link in db was failed");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during deleting video link");
                throw new Exception("Error during deleting video link", ex);
            }
        }

        public async Task<VideoLinkModel> GetVideoLink(int accountId, int contactId, string mediaId)
        {
            try
            {
                var dbResult = await _dbAccessor.GetVideoLink(accountId, contactId, mediaId);

                if (!dbResult.Result)
                {
                    _logger.LogInformation("Getting video link from db was failed");
                    throw new Exception("Getting video link from db was failed");
                }

                return dbResult;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting video link was failed");
                throw new Exception("Getting video link was failed", ex);
            }
        }
    }
}
