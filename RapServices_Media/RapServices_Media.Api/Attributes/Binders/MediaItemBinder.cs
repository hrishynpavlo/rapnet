﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using RapServices_Media.Api.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema.Generation;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using RapServices_Media.Api.Extensions;

namespace RapServices_Media.Api.Attributes.Binders
{
    public class MediaItemBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {            
            try
            {
                if (bindingContext == null)
                {
                    throw new ArgumentNullException(nameof(bindingContext));
                }

                if (bindingContext?.HttpContext?.Request?.Form?.Files?.Count() < 1)
                {
                    bindingContext.ModelState.AddModelError("File", "Form has no files");
                    return Task.CompletedTask;
                }

                if(bindingContext?.HttpContext?.Request?.Form?.Files?.FirstOrDefault()?.ContentType?.GetFileTypeByContentType() != "image")
                {
                    bindingContext.ModelState.AddModelError("File", "File type must be image");
                    return Task.CompletedTask;
                }

                var sizes = bindingContext.ValueProvider.GetValue("Sizes").FirstValue;
                if (String.IsNullOrEmpty(sizes) || sizes == "{}")
                {
                    bindingContext.ModelState.AddModelError("Sizes", "Sizes can't be null or empty");
                    return Task.CompletedTask;
                }

                var generator = new JSchemaGenerator();
                var schemaForSizeDictionary = generator.Generate(typeof(Dictionary<string, Size>));
                var sizesJson = JObject.Parse(sizes);
                var isSizesValidJson = sizesJson.IsValid(schemaForSizeDictionary);

                if (!isSizesValidJson)
                {
                    bindingContext.ModelState.AddModelError("Sizes", "Json is invalid. Lets try to use like this: " +
                        "{ 'large': { 'Width': 2, 'Height': 20 }, 'small': { 'Width': 2, 'Height': 20 } }");
                    return Task.CompletedTask;
                }

                var result = new MediaItemDTO
                {
                    File = bindingContext?.HttpContext?.Request?.Form?.Files?.FirstOrDefault(),
                    Sizes = JsonConvert.DeserializeObject<Dictionary<string, Size>>(bindingContext?.HttpContext?.Request?.Form?["Sizes"].FirstOrDefault()),
                    Metadata = bindingContext?.HttpContext?.Request?.Form?["Metadata"].FirstOrDefault(),
                    MediaName = bindingContext?.HttpContext?.Request?.Form?["MediaName"].FirstOrDefault(),
                    RapType = bindingContext?.HttpContext?.Request?.Form?["RapType"].FirstOrDefault()
                };

                bindingContext.Result = ModelBindingResult.Success(result);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.FromException(ex);
            }           
        }
    }
}
