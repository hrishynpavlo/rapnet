﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Accessors.StorageAccessor
{
    public class RemoveStorageResultModel
    {
        public bool Result { get; set; }
        public string GUID { get; set; }
    }
    public class StorageResultModel : RemoveStorageResultModel
    {        
        public Dictionary<string, string> ImagesLink { get; set; }  
    }   
}
