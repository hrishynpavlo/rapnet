﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Extensions;
using RapServices_Media.Api.Interfaces;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using ImageMagick;
using System.Linq;
using Amazon.Runtime;

namespace RapServices_Media.Api.Accessors.StorageAccessor
{
    public class S3StorageAccessor : IMediaServiceStorageAccessor
    {
        private readonly ILogger<S3StorageAccessor> _logger;
        private readonly AmazonS3Client _s3Client;
        private readonly string _bucketName;
        private readonly string _s3url;

        public S3StorageAccessor(ILogger<S3StorageAccessor> logger, IStorageProvider storageProvider, Amazon.RegionEndpoint regionEndpoint)
        {
            _logger = logger;
            _s3Client = new AmazonS3Client(new BasicAWSCredentials(storageProvider.AccessKey, storageProvider.SecretKey), regionEndpoint);
            _bucketName = storageProvider.BucketName;
            _s3url = storageProvider.Url;
        }

        public async Task<RemoveStorageResultModel> RemoveImagesAsync(string mediaItem)
        {
            try
            {
                var existingObjectsRequest = new ListObjectsRequest
                {
                    BucketName = _bucketName,
                    MaxKeys = 4,
                    Prefix = $"{mediaItem}/"
                };
                var existingImagesResult = await _s3Client.ListObjectsAsync(existingObjectsRequest);
                if (existingImagesResult.HttpStatusCode != System.Net.HttpStatusCode.OK) throw new Exception($"Image searching with mediaId = {mediaItem} was failed");

                var deleteRequest = new DeleteObjectsRequest
                {
                    BucketName = _bucketName,
                    Objects = existingImagesResult.S3Objects.Select(x => new KeyVersion { Key = x.Key }).ToList()
                };
                var deleteResult = await _s3Client.DeleteObjectsAsync(deleteRequest);
                if (deleteResult.HttpStatusCode != System.Net.HttpStatusCode.OK) throw new Exception($"Deleting was {mediaItem} failed");

                return new RemoveStorageResultModel { Result = true, GUID = mediaItem };
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Deleting was failed");
                throw new Exception("Deleting was failed", ex);
            }
        }

        public async Task<StorageResultModel> UploadImageAsync(MediaItemDTO mediaItem, string mediaItemId = null)
        {
            var tokenSource = new CancellationTokenSource();
            try
            {
                var imageByteArray = await mediaItem?.File?.GetBytesFromIFileAsync();
                mediaItem.Sizes.Add("original", new Size { Width = imageByteArray.GetWidth(), Height = imageByteArray.GetHeight() });
                var resizedImages = new Dictionary<string, MemoryStream>();

                using (MagickImage initialImage = new MagickImage(imageByteArray))
                {
                    foreach (var size in mediaItem.Sizes.Keys)
                    {
                        resizedImages.Add(size, await initialImage.ResizeImageAsync(mediaItem.Sizes[size]));
                        _logger.LogInformation($"Resizing is succesfully for {mediaItem.Sizes[size].ToString()}");
                    }    
                 }

                var fileExtension = Path.GetExtension(mediaItem?.File?.FileName);
                var imageLinks = new Dictionary<string, string>();
                var guid = mediaItemId??Guid.NewGuid().ToString();

                //delete existing images before upload new cropped
                if(mediaItemId != null)
                {
                    var removeResult = await RemoveImagesAsync(mediaItemId);

                    if (!removeResult.Result)
                    {
                        _logger.LogError("Deleting was failed and cancelled");
                        throw new Exception("Deleting was failed");
                    }
                }

                Parallel.ForEach(resizedImages, new ParallelOptions { MaxDegreeOfParallelism = 4, CancellationToken = tokenSource.Token }, keyValuePair =>
                {
                    var key = $"{guid}/{keyValuePair.Key}{fileExtension}";
                    var putRequest = new PutObjectRequest
                    {
                        BucketName = _bucketName,
                        Key = key,
                        InputStream = keyValuePair.Value,
                        CannedACL = S3CannedACL.PublicReadWrite,
                        TagSet = new List<Tag> { new Tag { Key = "Compressed", Value = "true" } }
                    };
                    putRequest.Metadata.Add("x-amz-meta-file-name", mediaItem?.File?.FileName);

                    var result = _s3Client.PutObjectAsync(putRequest).Result;
                    
                    if (result?.HttpStatusCode != System.Net.HttpStatusCode.OK) throw new Exception($"Uploading to S3 was failed for {key}");
                    imageLinks.Add(keyValuePair.Key, $"{_s3url}/{_bucketName}/{key}");
                    _logger.LogInformation($"{key} is uploaded");
                });

                return new StorageResultModel { Result = true, ImagesLink = imageLinks, GUID = guid };
            }
            catch (Exception ex)
            {
                tokenSource.Cancel();
                _logger.LogError(ex, "Uploading was failed and cancelled");
                throw new Exception("Uploading was failed", ex);
            }
        }
    }
}
