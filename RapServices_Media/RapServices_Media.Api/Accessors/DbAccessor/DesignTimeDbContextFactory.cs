﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RapServices_Media.Api.Accessors.DbAccessor
{
    
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MediaServiceContext>
    {
        public MediaServiceContext CreateDbContext(string[] args)
        {
            string connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            DbContextOptionsBuilder<MediaServiceContext> builder = new DbContextOptionsBuilder<MediaServiceContext>();
            builder.UseNpgsql(connectionString);

            return new MediaServiceContext(builder.Options);
        }
    }
}
