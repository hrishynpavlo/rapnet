﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Extensions;
using RapServices_Media.Api.Interfaces;
using RapServices_Media.Api.Accessors.DbAccessor;
using RapServices_Media.Api.Accessors.DbAccessor.DO;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using RapServices_Media.Api.Accessors.StorageAccessor;
using System.Threading;
using System.Data.Common;
using System.Linq;
using System.Collections.Generic;
using RapServices_Media.Api.Constants;
using System.IO;

namespace RapServices_Media.Api.Accessor
{
    /// <summary>
    /// Implementation for IMediaServiceDbAccessor using Postgresql
    /// </summary>
    public class MediaServiceDbAccessor : IMediaServiceDbAccessor
    {
        private readonly ILogger<MediaServiceDbAccessor> _logger;
        private readonly MediaServiceContext _context;
        /// <summary>
        /// MediaServiceDbAccessor constructor
        /// </summary>
        /// <param name="context">MediaServiceContext</param>
        /// <param name="logger">ILogger<MediaServiceDbAccessor></param>
        /// <returns>MediaServiceDbAccessor element</returns>
        public MediaServiceDbAccessor(MediaServiceContext context, ILogger<MediaServiceDbAccessor> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<DbResultModel> SaveImageAsync(int accountId, int contactId, MediaItemDTO item, StorageResultModel storageResult)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var mediaId = storageResult.GUID;
                    var newMediaItem = new Media
                    {
                        MediaId = mediaId,
                        AccountId = accountId,
                        ContactId = contactId,
                        Metadata = JsonConvert.SerializeObject(item.Metadata),
                        MediaType = MediaTypes.Image,                       
                        RapType = item.RapType,
                        MediaName = item.MediaName               
                    };
                    _context.Media.Add(newMediaItem);

                    foreach (var imageKey in storageResult.ImagesLink.Keys)
                    {
                        var dbImage = new Image
                        {
                            MediaId = mediaId,
                            Width = item.Sizes[imageKey].Width,
                            Hight = item.Sizes[imageKey].Height,
                            Link = storageResult.ImagesLink[imageKey],
                            FileName = item.File.FileName.Substring(0, item.File.FileName.IndexOf(".")),
                            FileType = Path.GetExtension(item.File.FileName),
                            MimeType = item.File.ContentType,
                            Resolution = imageKey
                        };

                        _context.Image.Add(dbImage);
                    }

                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return new DbResultModel { Result = true, MediaId = mediaId };
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Adding item to db was failed");
                    transaction.Rollback();
                    throw new Exception("Adding item to db was failed", ex);
                }
            }
        }
        public async Task<DbResultModel> EditImageAsync(string mediaItemId, MediaItemDTO item, StorageResultModel storageResult)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbMediaItem = await _context.Media.FirstOrDefaultAsync(x => x.MediaId == mediaItemId);
                    if (dbMediaItem == null)
                    {
                        _logger.LogInformation($"Item {mediaItemId} wasn't found in db");
                        throw new Exception($"Item with Id = {mediaItemId} wasn't found in the database");
                    }

                    dbMediaItem.Metadata = JsonConvert.SerializeObject(item.Metadata);
                    //dbMediaItem.MediaType = MediaTypes.Image;
                    dbMediaItem.MediaName = item.MediaName;
                    dbMediaItem.RapType = item.RapType;

                    _context.Image.RemoveRange(dbMediaItem.Images);

                    foreach (var imageKey in storageResult.ImagesLink.Keys)
                    {
                        var dbImage = new Image
                        {
                            MediaId = mediaItemId,
                            Width = item.Sizes[imageKey].Width,
                            Hight = item.Sizes[imageKey].Height,
                            Link = storageResult.ImagesLink[imageKey],
                            FileName = item.File.FileName.Substring(0, item.File.FileName.IndexOf(".")),
                            FileType = Path.GetExtension(item.File.FileName),
                            MimeType = item.File.ContentType,
                            Resolution = imageKey
                        };

                        _context.Image.Add(dbImage);
                    }

                    _context.Update(dbMediaItem);
                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return new DbResultModel { Result = true, MediaId = mediaItemId };
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Updating item from db was failed");
                    transaction.Rollback();
                    throw new Exception($"Updating item {mediaItemId} from db was failed", ex);
                }
            }
        }
        public async Task<DbResultModel> DeleteImageAsync(string mediaItemId)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbMediaItem = await _context.Media.FirstOrDefaultAsync(x => x.MediaId == mediaItemId);
                    if (dbMediaItem == null)
                    {
                        _logger.LogInformation($"Item {mediaItemId} wasn't found in db");
                        throw new Exception($"Item with Id = {mediaItemId} wasn't found in the database");
                    }

                    _context.Image.RemoveRange(dbMediaItem.Images);
                    _context.Media.RemoveRange(dbMediaItem);

                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return new DbResultModel { Result = true, MediaId = mediaItemId };
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Removing item {mediaItemId} from db was failed");
                    transaction.Rollback();
                    throw new Exception($"Removing item {mediaItemId} from db was failed", ex);
                }
            }
        }
        public MediaLinks GetSingleMediaImageLinks(string mediaItemId)
        {
            try
            {
                var dbMediaItems = _context.Image.AsNoTracking().Where(x => x.MediaId == mediaItemId);
                if (dbMediaItems?.Count() < 1) throw new Exception("Items wasn't found in database");

                return new MediaLinks
                {
                    Result = true,
                    MediaId = mediaItemId,
                    ImageLinks = dbMediaItems.Select(s => new { Alias = s.Resolution, Link = s.Link }).ToDictionary(d => d.Alias, d => d.Link)
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting item from db was failed");
                throw new Exception($"Getting item {mediaItemId} from db was failed", ex);
            }
        }

        public ListMediaLinks GetMultipleImagesLinks(MediaRequestModel mediaItemIds)
        {
            try
            {
                mediaItemIds.MediaAliasIds.Where(x => x.AliasIds == null || x.AliasIds.Count() == 0).ToList().ForEach(f =>
                {
                    f.AliasIds = new List<string> { "original", "small", "large", "alias" };
                });

                var dbQuery = _context.Image.AsNoTracking().
                    Where(x => mediaItemIds.MediaAliasIds.Any(a => a.MediaId == x.MediaId && a.AliasIds.Contains(x.Resolution)))
                    .Select(s => new { MediaId = s.MediaId, Alias = s.Resolution, Link = s.Link }).GroupBy(x => x.MediaId).AsEnumerable();

                return new ListMediaLinks
                {
                    Result = true,
                    Links = dbQuery.Select(s => new MediaLinks
                    {
                        MediaId = s.Key,
                        ImageLinks = s.ToList().ToDictionary(d => d.Alias, d => d.Link)
                    }).ToList()
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting items from db was failed");
                throw new Exception($"Getting items {mediaItemIds.ToString()} from db was failed", ex);
            }
        }
    }
}
