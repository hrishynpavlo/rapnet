﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapServices_Media.Api.Accessors.DbAccessor.DO
{
    public  class Media
    {
        [Key]
        public string MediaId { get; set; }
        public string MediaType { get; set; }
        public string Metadata { get; set; }
        public string RapType { get; set; }
        public string MediaName { get; set; }
        public int AccountId { get; set; }
        public int ContactId { get; set; }

        public ICollection<Video>  Videos { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}
