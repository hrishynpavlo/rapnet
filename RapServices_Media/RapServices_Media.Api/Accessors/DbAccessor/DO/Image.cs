﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapServices_Media.Api.Accessors.DbAccessor.DO
{
    public  class Image
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ImageId { get; set; }
        [ForeignKey("Media")]
        public string MediaId { get; set; }
        public string Resolution { get; set; }
        public int Hight { get; set; }
        public int Width { get; set; }
        public string Link { get; set; }
        public string FileType { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }

        public Media Media { get; set; }
      
    }
}
