﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RapServices_Media.Api.Accessors.DbAccessor.DO
{
    public class Video
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VideoId { get; set; }
        [ForeignKey("Media")]
        public string MediaId { get; set; }
        [ForeignKey("VideoProvider")]
        public int ProviderId { get; set; }
        public string Link { get; set; }

        public Media Media { get; set; }
        public VideoProvider Provider { get; set; }
    }
}
