﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Accessors.DbAccessor
{
    public class DbResultModel
    {
        public bool Result { get; set; }
        public string MediaId { get; set; }
    }
    public class ListMediaLinks
    {
        public bool Result { get; set; }
        public List<MediaLinks> Links { get; set; }
    }
    public class MediaLinks {
        public string MediaId { get; set; }
        public Dictionary<string, string> ImageLinks { get; set; }
        [JsonIgnore]
        public bool Result { get; set; }
    }
}
