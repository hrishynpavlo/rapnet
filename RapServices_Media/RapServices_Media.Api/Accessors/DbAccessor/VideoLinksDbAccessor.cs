﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RapServices_Media.Api.DTO;
using RapServices_Media.Api.Interfaces;
using RapServices_Media.Api.Accessors.DbAccessor;
using RapServices_Media.Api.Accessors.DbAccessor.DO;
using RapServices_Media.Api.Constants;

namespace RapServices_Media.Api.Accessor
{
    public class VideoLinksDbAccessor : IVideoLinksDbAccessor
    {
        private readonly ILogger<MediaServiceDbAccessor> _logger;
        private readonly MediaServiceContext _context;

        public VideoLinksDbAccessor(MediaServiceContext context, ILogger<MediaServiceDbAccessor> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<bool> SaveProviderAsync(string providerLink)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {

                    _context.VideoProvider.Add(new VideoProvider{ Name = providerLink});
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Adding provider to db was failed");
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<bool> EditProviderAsync(VideoProviderRequestDTO provider)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbProvider = _context.VideoProvider.FirstOrDefault(x => x.ProviderId == provider.Id);
                    if (dbProvider != null)
                    {
                        dbProvider.Name = provider.ProviderLink;

                        _context.VideoProvider.Update(dbProvider);
                        await _context.SaveChangesAsync();

                        transaction.Commit();

                        return true;
                    }
                    _logger.LogInformation("Provider wasn't found in db");
                    return false;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Updating provider in db was failed");
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<bool> DeleteProviderAsync(short providerId)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbProvider = _context.VideoProvider.FirstOrDefault(x => x.ProviderId == providerId);
                    if (dbProvider != null)
                    {
                        _context.VideoProvider.Remove(dbProvider);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    _logger.LogInformation("Provider wasn't found in db");
                    return false;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Removing provider from db was failed");
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<List<VideoProvider>> GetProviders()
        {
            try
            {
                var dbProviders = _context.VideoProvider.ToList();
                return dbProviders.Any() ? dbProviders : new List<VideoProvider>();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fetching providers from db was failed");
                return new List<VideoProvider>();
            }
        }

        public async Task<DbResultModel> SaveLinkAsync(int accountId, int contactId, string link)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var providers = GetProviders().Result;
                    if (providers.Any(x => link.ToLower().StartsWith(x.Name.ToLower())))
                    {
                        var mediaId = Guid.NewGuid().ToString();
                        var newMedia = new Media
                        {
                            MediaId = mediaId,
                            MediaType = MediaTypes.Video,
                            Metadata = string.Empty,
                            RapType = string.Empty,
                            AccountId = accountId,
                            ContactId = contactId
                        };
                        _context.Media.Add(newMedia);

                        var newVideo = new Video
                        {
                            MediaId = mediaId,
                            ProviderId = providers.FirstOrDefault(x => link.ToLower().StartsWith(x.Name.ToLower())).ProviderId,
                            Link = link
                        };

                        _context.Video.Add(newVideo);
                        await _context.SaveChangesAsync();

                        transaction.Commit();

                        return new DbResultModel { Result = true, MediaId = mediaId };
                    }

                    _logger.LogInformation("Adding video link to db was failed(Unsupported provider)");
                    return new DbResultModel { Result = false };
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Adding video link to db was failed");
                    transaction.Rollback();
                    return new DbResultModel { Result = false };
                }
            }
        }

        public async Task<bool> EditLinkAsync(int accountId, int contactId, VideoLinkRequestDTO link)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var providers = GetProviders().Result;
                    var dbVideo = _context.Video.FirstOrDefault(x => x.MediaId == link.Id);

                    if (providers.Any(x => link.Link.ToLower().StartsWith(x.Name.ToLower())) && dbVideo != null)
                    {
                        dbVideo.ProviderId = providers.FirstOrDefault(x => link.Link.ToLower().StartsWith(x.Name.ToLower())).ProviderId;
                        dbVideo.Link = link.Link;

                        _context.Video.Update(dbVideo);
                        await _context.SaveChangesAsync();

                        transaction.Commit();

                        return true;
                    }

                    _logger.LogInformation("Editing video link in db was failed(Unsupported provider)");
                    return false;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Editing video link in db was failed");
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<bool> DeleteLinkAsync(int accountId, int contactId, string linkId)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbVideo = _context.Video.FirstOrDefault(x => x.MediaId == linkId);
                    var dbMedia = _context.Media.FirstOrDefault(x => x.MediaId == linkId);

                    if (dbVideo != null && dbMedia != null)
                    {
                        _context.Video.Remove(dbVideo);
                        _context.Media.Remove(dbMedia);

                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                    _logger.LogInformation("Video link wasn't found in db");
                    return false;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Removing video link from db was failed");
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<VideoLinkModel> GetVideoLink(int accountId, int contactId, string mediaItemId)
        {
            try
            {
                var dbMediaItems = _context.Video.AsNoTracking().FirstOrDefault(x => x.MediaId == mediaItemId);
                if (dbMediaItems == null) throw new Exception("Items wasn't found in database");

                return new VideoLinkModel
                {
                    Result = true,
                    MediaId = mediaItemId,
                    VideoLink = dbMediaItems.Link
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting video link from db was failed");
                throw new Exception($"Getting video link with Id '{mediaItemId}' from db was failed", ex);
            }
        }
    }
}
