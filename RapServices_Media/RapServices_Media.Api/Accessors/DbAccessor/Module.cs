﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.EntityFrameworkCore;

namespace RapServices_Media.Api.Accessors.DbAccessor
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MediaServiceContext>().AsSelf().AsImplementedInterfaces();

            DbContextOptions<MediaServiceContext> dbOptions = CreateDbOptions();
            builder.RegisterInstance(dbOptions);
        }

        private DbContextOptions<MediaServiceContext> CreateDbOptions()
        {
            string connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            DbContextOptions<MediaServiceContext> dbOptions = new DbContextOptionsBuilder<MediaServiceContext>().UseNpgsql(connectionString).Options;
            return dbOptions;
        }
    }
}
