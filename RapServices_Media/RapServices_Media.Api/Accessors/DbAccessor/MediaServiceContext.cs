﻿using System;
using Microsoft.EntityFrameworkCore;
using RapServices_Media.Api.Accessors.DbAccessor.DO;

namespace RapServices_Media.Api.Accessors.DbAccessor
{
    public partial class MediaServiceContext : DbContext
    {
        public MediaServiceContext()
        {
        }

        public MediaServiceContext(DbContextOptions<MediaServiceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Image> Image { get; set; }
        
        public virtual DbSet<Media> Media { get; set; }
        
        public virtual DbSet<Video> Video { get; set; }
        public virtual DbSet<VideoProvider> VideoProvider { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // optionsBuilder.UseNpgsql(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING_LOCAL"));
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure default schema
            modelBuilder.HasDefaultSchema("mediaDB");
        }
    }
}
