﻿using RapServices_Media.Api.Interfaces;
using System;

namespace RapServices_Media.Api.Providers
{
    public class AwsProvider : IStorageProvider
    {
        public string BucketName => Environment.GetEnvironmentVariable("S3_BUCKET_NAME");
        public string Url => Environment.GetEnvironmentVariable("S3_URL");
        public string AccessKey => Environment.GetEnvironmentVariable("AWS_ACCESS_KEY");
        public string SecretKey => Environment.GetEnvironmentVariable("AWS_SECRET_KEY");
    }
}
