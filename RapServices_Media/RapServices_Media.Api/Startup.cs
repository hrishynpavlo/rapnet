﻿using System;
using System.IO;
using Amazon.Runtime;
using Amazon.S3;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using RapServices_Media.Api.Accessor;
using RapServices_Media.Api.Accessors.StorageAccessor;
using RapServices_Media.Api.Interfaces;
using RapServices_Media.Api.Managers;
using RapServices_Media.Api.Providers;
using Swashbuckle.AspNetCore.Swagger;

namespace RapServices_Media.Api
{
    public class Startup
    {
		public Autofac.IContainer ApplicationContainer { get; private set; }
		public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
			services.AddCors();
			services.AddMvc();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "Media Service API", Version = "v1" });
				var basePath = PlatformServices.Default.Application.ApplicationBasePath;
				var xmlPath = Path.Combine(basePath, $"{PlatformServices.Default.Application.ApplicationName}.xml");
				c.IncludeXmlComments(xmlPath);
			});

            services.AddSingleton<IMediaServiceManager, MediaManager>();
            services.AddSingleton<IMediaServiceStorageAccessor, S3StorageAccessor>();
            services.AddSingleton<IMediaServiceDbAccessor, MediaServiceDbAccessor>();
            services.AddSingleton<IVideoLinksManager, VideoLinksManager>();
            services.AddSingleton<IVideoLinksDbAccessor, VideoLinksDbAccessor>();
            services.AddSingleton<IStorageProvider, AwsProvider > ();           
            services.AddSingleton<Amazon.RegionEndpoint>(p => Amazon.RegionEndpoint.USEast1);

            var builder = CreateAndInitializeContainerBuilder(services);

			ApplicationContainer = builder.Build();

			return new AutofacServiceProvider(ApplicationContainer);
		}


		private ContainerBuilder CreateAndInitializeContainerBuilder(IServiceCollection services)
		{
			var builder = new ContainerBuilder();

            builder.RegisterModule<Accessors.DbAccessor.Module>();

            builder.Populate(services);
			return builder;
		}
		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
			app.UseMvc();
			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "RapNet System Messages Service Service API V1");
			});
		}
    }
}
