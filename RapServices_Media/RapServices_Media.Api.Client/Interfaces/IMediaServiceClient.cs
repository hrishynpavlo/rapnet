﻿using RapServices_Media.Api.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RapServices_Media.Api.Client.Interfaces
{
    public interface IMediaServiceClient
    {
        Task<MediaResponseModelDTO> UploadFile(HttpContent file, string path = null);
    }
}
