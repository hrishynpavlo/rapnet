﻿using System.Net.Http;
using System.Threading.Tasks;
using Infrastructure.RestClient;
using RapServices_Media.Api.Client.Interfaces;
using RapServices_Media.Api.DTO;

namespace RapServices_Media.Api.Client
{
    public class MediaServiceClient : ClientApi, IMediaServiceClient
    {
        private const string ApiPath = "api/Media";
        public MediaServiceClient(string serviceUri) : base(serviceUri)
        {

        }

        /// <summary>
        /// Get result of uploading file
        /// </summary>
        /// <param name="file">HttpContent</param>
        /// <param name="path">string</param>
        /// <returns>ResponseModel with uploading result</returns>
        public async Task<MediaResponseModelDTO> UploadFile(HttpContent file, string path = null)
        {
            var request = RequestBuildExtensions.CreatePostRequest($"{ApiPath}/UploadFile");

            var filename = file.Headers.ContentDisposition.FileName.Trim('\"');

            var fileBytes = await file.ReadAsByteArrayAsync();

            request.AddFileBytes(filename, fileBytes, filename, file.Headers.ContentType.MediaType);

            return await ExecuteRequestAsync<MediaResponseModelDTO>(request);
        }
    }
}
